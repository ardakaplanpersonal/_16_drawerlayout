package com.androidegitim.androidegitimlibrary.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class DialogHelpers {

    private DialogHelpers() {

    }

    public static void showDialog(Activity activity, String dialogTitle, String dialogMessage,
                                  String positiveButtonText, DialogInterface.OnClickListener positiveClickListener,
                                  String negativeButtonText, DialogInterface.OnClickListener negativeClickListener,
                                  String neutralButtonText, DialogInterface.OnClickListener neutralClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(dialogTitle);

        builder.setMessage(dialogMessage);

        builder.setPositiveButton(positiveButtonText, positiveClickListener);

        if (negativeButtonText != null) {

            builder.setNegativeButton(negativeButtonText, negativeClickListener);
        }

        if (neutralButtonText != null) {

            builder.setNeutralButton(neutralButtonText, neutralClickListener);
        }

        builder.show();
    }
}