package com.androidegitim.drawerlayout.models;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.androidegitim.androidegitimlibrary.helpers.FragmentHelpers;
import com.androidegitim.drawerlayout.R;
import com.androidegitim.drawerlayout.ui.fragments.AboutFragment;
import com.androidegitim.drawerlayout.ui.fragments.ContactFragment;
import com.androidegitim.drawerlayout.ui.fragments.HomePageFragment;
import com.androidegitim.drawerlayout.ui.fragments.LoginFragment;

import java.util.ArrayList;

/**
 * Created by Arda Kaplan on 2.01.2018 - 10:08
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class LeftMenuItem {

    private String name;
    private int drawableID;
    private View.OnClickListener clickListener;

    private LeftMenuItem(String name, int drawableID, View.OnClickListener clickListener) {
        this.name = name;
        this.drawableID = drawableID;
        this.clickListener = clickListener;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableID() {
        return drawableID;
    }

    public void setDrawableID(int drawableID) {
        this.drawableID = drawableID;
    }

    public static ArrayList<LeftMenuItem> getAll(final DrawerLayout drawerLayout, final AppCompatActivity appCompatActivity) {

        ArrayList<LeftMenuItem> leftMenuItems = new ArrayList<>();

        leftMenuItems.add(new LeftMenuItem("Anasayfa", R.drawable.icon_hompage, new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawers();

                FragmentHelpers.openFragment(appCompatActivity, new HomePageFragment(), R.id.main_linearlayout_fragment_part);
            }
        }));

        leftMenuItems.add(new LeftMenuItem("İletişim", R.drawable.icon_contact, new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawers();

                FragmentHelpers.openFragment(appCompatActivity, new ContactFragment(), R.id.main_linearlayout_fragment_part);
            }
        }));

        leftMenuItems.add(new LeftMenuItem("Giriş", R.drawable.icon_login, new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawers();

                FragmentHelpers.openFragment(appCompatActivity, new LoginFragment(), R.id.main_linearlayout_fragment_part);
            }
        }));

        leftMenuItems.add(new LeftMenuItem("Hakkında", R.drawable.icon_about, new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                drawerLayout.closeDrawers();

                FragmentHelpers.openFragment(appCompatActivity, new AboutFragment(), R.id.main_linearlayout_fragment_part);
            }
        }));

        return leftMenuItems;
    }
}
