package com.androidegitim.drawerlayout.ui.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidegitim.drawerlayout.R;
import com.androidegitim.drawerlayout.models.LeftMenuItem;

import java.util.List;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class LeftMenuListViewAdapter extends BaseAdapter {

    private Activity activity;
    private List<LeftMenuItem> dataList;

    public LeftMenuListViewAdapter(Activity activity, List<LeftMenuItem> dataList) {
        this.activity = activity;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public LeftMenuItem getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = LayoutInflater.from(activity).inflate(R.layout.row_left_menu_item, parent, false);
        }

        LeftMenuItem item = getItem(position);

        TextView textView = convertView.findViewById(R.id.row_left_menu_item_textview_name);
        {
            textView.setText(item.getName());
        }

        ImageView imageView = convertView.findViewById(R.id.row_left_menu_item_imageview_image);
        {
            imageView.setImageResource(item.getDrawableID());
        }

        convertView.setOnClickListener(item.getClickListener());

        return convertView;
    }
}
