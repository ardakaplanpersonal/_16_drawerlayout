package com.androidegitim.drawerlayout.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidegitim.drawerlayout.R;
import com.androidegitim.drawerlayout.ui.activities.MainActivity;

/**
 * Created by Arda Kaplan on 2.01.2018 - 10:33
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class AboutFragment extends Fragment {


    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).setTitle("HAKKINDA");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_about, container, false);
    }
}
