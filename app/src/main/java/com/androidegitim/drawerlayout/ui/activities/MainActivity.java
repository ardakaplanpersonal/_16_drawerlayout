package com.androidegitim.drawerlayout.ui.activities;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.androidegitim.androidegitimlibrary.helpers.FragmentHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.drawerlayout.R;
import com.androidegitim.drawerlayout.models.LeftMenuItem;
import com.androidegitim.drawerlayout.ui.adapters.LeftMenuListViewAdapter;
import com.androidegitim.drawerlayout.ui.fragments.HomePageFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_listview_left_menu)
    ListView leftMenuList;
    @BindView(R.id.main_drawerlayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.main_textview_title)
    TextView titleTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        RDALogger.start("DRAWER LAYOUT").enableLogging(true);

        FragmentHelpers.openFragment(this, new HomePageFragment(), R.id.main_linearlayout_fragment_part);

        leftMenuList.setAdapter(new LeftMenuListViewAdapter(this, LeftMenuItem.getAll(drawerLayout, this)));
    }

    public void setTitle(String title) {

        titleTextView.setText(title);
    }

    @OnClick(R.id.main_imageview_drawer)
    public void adjustDrawer() {

        if (drawerLayout.isDrawerOpen(Gravity.START)) {

            drawerLayout.closeDrawers();

        } else {

            drawerLayout.openDrawer(Gravity.START);
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(Gravity.START)) {

            drawerLayout.closeDrawers();

        } else {

            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

                finish();

            } else {

                super.onBackPressed();
            }
        }
    }
}